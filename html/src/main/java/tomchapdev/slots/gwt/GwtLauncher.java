package tomchapdev.slots.gwt;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import com.google.gwt.core.client.GWT;

import tomchapdev.slots.SlotMachine;

/** Launches the GWT application. */
public class GwtLauncher extends GwtApplication {
        @Override
        public GwtApplicationConfiguration getConfig () {
            // Resizable application, uses available space in browser
            GwtApplicationConfiguration cfg = new GwtApplicationConfiguration(true);
            return cfg;
        }

        @Override
        public ApplicationListener createApplicationListener () {
            return new SlotMachine();
        }
}
