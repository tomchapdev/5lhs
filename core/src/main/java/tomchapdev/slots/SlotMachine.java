package tomchapdev.slots;

import com.badlogic.gdx.Game;

import tomchapdev.slots.states.MenuScreen;

public class SlotMachine extends Game {
    @Override
    public void create() {
        setScreen(new MenuScreen(this, MenuScreen.AudioState.FULL));
    }
}
