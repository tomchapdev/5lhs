package tomchapdev.slots;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import tomchapdev.slots.states.MenuScreen;

public class DefaultScreen implements Screen {
    public enum AudioState {FULL, HALF, OFF}
    protected AudioState audioState;
    protected final Sprite audioButton;
    public Sound confirmSound;
    public Sound stopSound;
    public Sound winSound;
    public Sound loseSound;
    public Sound spinSound;
    public Sound holdSound;
    public Music bgMusic;
    protected Game game;
    protected OrthographicCamera camera;
    protected Viewport viewport;
    public final Vector2 screenSize = new Vector2(1920, 1080);

    public DefaultScreen(Game game, boolean menuMusic, AudioState currentAudioState) {
        this.game = game;
        this.audioState = currentAudioState;

        audioButton = new Sprite(new Texture(Gdx.files.internal("art/ui2.png")));
        audioButton.setSize(218, 206);

        if (menuMusic)
            bgMusic = Gdx.audio.newMusic(Gdx.files.internal("audio/Funky-Pixel-Town_Looping.mp3"));
        else
            bgMusic = Gdx.audio.newMusic(Gdx.files.internal("audio/Too-Cool-for-Words.mp3"));
        bgMusic.setLooping(true);
        bgMusic.play();

        if (audioState == MenuScreen.AudioState.FULL) {
            audioButton.setRegion(225, 600, 218, 206);
            bgMusic.setVolume(0.8f);
        } else if (audioState == MenuScreen.AudioState.HALF) {
            audioButton.setRegion(443, 600, 218, 206);
            bgMusic.setVolume(0.4f);
        } else { // audioState == AudioState.OFF
            audioButton.setRegion(661, 600, 218, 206);
            bgMusic.setVolume(0.f);
        }

        confirmSound = Gdx.audio.newSound(Gdx.files.internal("audio/Confirm.ogg"));
        stopSound = Gdx.audio.newSound(Gdx.files.internal("audio/Stop.ogg"));
        winSound = Gdx.audio.newSound(Gdx.files.internal("audio/Win.ogg"));
        loseSound = Gdx.audio.newSound(Gdx.files.internal("audio/Lose.ogg"));
        spinSound = Gdx.audio.newSound(Gdx.files.internal("audio/Spin.ogg"));
        holdSound = Gdx.audio.newSound(Gdx.files.internal("audio/Hold.ogg"));

        camera = new OrthographicCamera();
        camera.setToOrtho(false, screenSize.x, screenSize.y);
        viewport = new FitViewport(screenSize.x, screenSize.y, camera);
        viewport.apply();
    }

    // Plays a sound according to audio state (player toggled volume settings)
    public void playSound(Sound sound, boolean loop) {
        // Plays at full volume by default, so we only make changes if audioState is not FULL
        if (audioState != AudioState.OFF) {
            float volume;

            if (audioState == AudioState.FULL)
                volume = 1.f;
            else // audioState == AudioState.HALF
                volume = 0.5f;

            if (loop)
                sound.loop(volume);
            else
                sound.play(volume);
        }
    }

    // Switches between audio states
    protected void toggleAudio() {
        if (audioState == AudioState.FULL) {
            audioState = AudioState.HALF;
            audioButton.setRegion(443, 600, 218, 206);
            bgMusic.setVolume(0.4f);
        } else if (audioState == AudioState.HALF) {
            audioState = AudioState.OFF;
            audioButton.setRegion(661, 600, 218, 206);
            bgMusic.setVolume(0.f);
        } else if (audioState == AudioState.OFF) {
            audioState = AudioState.FULL;
            audioButton.setRegion(225, 600, 218, 206);
            bgMusic.setVolume(0.8f);
        }
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, true);
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {}

    @Override
    public void show() {}

    @Override
    public void hide() {}

    @Override
    public void render(float delta) {}

    @Override
    public void dispose() {
        confirmSound.dispose();
        stopSound.dispose();
        winSound.dispose();
        loseSound.dispose();
        spinSound.dispose();
        holdSound.dispose();
        bgMusic.dispose();
        game.dispose();
    }

}
