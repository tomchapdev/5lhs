package tomchapdev.slots.gameplay;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;

public class LightStrip {
    enum State {STANDBY, SPINNING, WIN, LOSE}

    private State state = State.STANDBY;
    private boolean[] lights;
    private final int numLights = 12;
    private final Sprite sprite;
    private final Vector2 startPos;
    private int position = 0;
    private long standbyStartTime;
    private long startTime;
    private long endTime;
    private long loopStartTime;
    private long loopTime;
    boolean winFlip = true;

    public LightStrip(Texture texture, Vector2 reelUiTop) {
        lights = new boolean[numLights];

        sprite = new Sprite(texture);
        sprite.setRegion(0, 600, 75, 75);
        sprite.setSize(75, 75);

        reelUiTop.x -= 2 + (75.f / 2.f); //11 fit almost exactly over the reelUI, 12 is a bit over
        reelUiTop.y += 10;
        startPos = reelUiTop;

        standbyStartTime = TimeUtils.nanoTime();
    }

    // Sets state and sets new time values for dancing lights
    public void changeState(State newState) {
        state = newState;
        startTime = TimeUtils.nanoTime();
        loopStartTime = startTime;

        // Set loop time according to each sound's bpm
        if (state == State.SPINNING) {
            loopTime = 62000000; // 0.062 seconds, approx time between each note in spinning sound
            // Spin is ended in SlotsGame update
            moveLights(true);
        } else if (state == State.WIN) {
            loopTime = 62000000; // 0.062 seconds, approx time between each note in win sound
            endTime = 869000000; // 0.869 seconds, approx win sound time
            winFlip = true;
            moveLights(true);
        } else if (state == State.LOSE) {
            loopTime = 376000000; // 0.376 seconds, approx time between each note in lose sound
            endTime = 1429000000; // 1.429 seconds, approx lose sound time
            moveLights(false);
        }
    }

    public void update() {
        final long standbyLoopTime = (long) 60 * 1000000000 / 126;

        // Always update the standby timer, hopefully it will stay in sync with the music
        if (TimeUtils.nanoTime() - standbyStartTime >= standbyLoopTime) {
            standbyStartTime += standbyLoopTime;

            if (state == State.STANDBY)
                moveLights(true);
        }

        if (state != State.STANDBY) {
            if (TimeUtils.nanoTime() - loopStartTime >= loopTime) {
                loopStartTime += loopTime;

                if (state == State.WIN) {
                    moveLights(winFlip);
                    winFlip = !winFlip;
                }
                else if (state == State.LOSE)
                    moveLights(false);
                else // Spinning
                    moveLights(true);
            }

            // Resets to standby sequence after win or lose sound has ended, spinning is stopped in slots update
            if (state != State.SPINNING && TimeUtils.nanoTime() - startTime >= endTime)
                state = State.STANDBY; // No need to call changeState as standby is independent
        }
    }

    // Moves all the active lights by one position, specified right or left
    private void moveLights(boolean right) {
        final int activeLights = 4;
        final int gap = (numLights / activeLights) - 1;

        if (right)
            position++;
        else
            position--;

        // Clamp position to array
        if (position > numLights - 1)
            position -= numLights;
        else if (position < 0)
            position += numLights;

        lights = new boolean[numLights]; // Reset array

        for (int i = 0; i < activeLights; ++i) {
            int temp = i + (i * gap) + position;

            if (temp > numLights - 1) // Clamp temp to array
                temp -= numLights;

            lights[temp] = true;
        }
    }

    public void render(SpriteBatch batch) {
        final int size = 75;
        final int yPos = 600;
        Vector2 pos = new Vector2(startPos);
        sprite.setPosition(pos.x, pos.y);

        if (lights[0]) {
            sprite.setRegion(0, yPos + size, size, size);
            sprite.draw(batch);
        } else {
            sprite.setRegion(0, yPos, size, size);
            sprite.draw(batch);
        }

        pos.x += size;
        sprite.setPosition(pos.x, pos.y);

        for (int i = 1; i < numLights - 1; ++i) {
            if (lights[i]) {
                sprite.setRegion(size, yPos + size, size, size);
                sprite.draw(batch);
            } else {
                sprite.setRegion(size, yPos, size, size);
                sprite.draw(batch);
            }

            pos.x += size;
            sprite.setPosition(pos.x, pos.y);
        }

        if (lights[numLights - 1]) {
            sprite.setRegion(size * 2, yPos + size, size, size);
            sprite.draw(batch);
        } else {
            sprite.setRegion(size * 2, yPos, size, size);
            sprite.draw(batch);
        }
    }
}
