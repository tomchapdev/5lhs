package tomchapdev.slots.gameplay;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

public class HoldButton extends Button {
    public HoldButton(Texture texture, Vector2 pos) {
        sprite = new Sprite(texture);

        Vector2 offset = new Vector2(31, 68);
        sprite.setSize(212, 148);
        sprite.setPosition(pos.x - offset.x, pos.y - offset.y);

        setState(State.DIM);
    }

    @Override
    public void setState(State newState) {
        state = newState;
        int offset = 0;

        if (state == State.DIM)
            offset += 148;
        else if (state == State.PRESSED)
            offset += 148 * 2;

        sprite.setRegion(0, 579 + offset, 212, 148);
    }
}
