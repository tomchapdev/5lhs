package tomchapdev.slots.gameplay;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

public abstract class Button {
    public enum State {LIT, DIM, PRESSED}
    protected State state;
    public Sprite sprite;

    abstract public void setState(State newState);

    public boolean isPressable() { return state == State.LIT; }

    public boolean contains(Vector2 pos) { return sprite.getBoundingRectangle().contains(pos); }
}
