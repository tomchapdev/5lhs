package tomchapdev.slots.gameplay;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

public class SpinButton extends Button {
    public SpinButton(Texture texture, Vector2 pos) {
        sprite = new Sprite(texture);

        Vector2 offset = new Vector2(-50, 185);
        sprite.setSize(212, 193);
        sprite.setPosition(pos.x - offset.x, pos.y - offset.y);

        setState(State.LIT);
    }

    @Override
    public void setState(State newState) {
        state = newState;
        int offset = 0;

        if (state == State.DIM)
            offset += 193;
        else if (state == State.PRESSED)
            offset += 193 * 2;

        sprite.setRegion(0, offset, 212, 193);
    }
}
