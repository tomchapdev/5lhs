package tomchapdev.slots.gameplay;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.TimeUtils;

import tomchapdev.slots.states.GameScreen;

public class SlotsGame {
    private final GameScreen gameScreen;

    enum InfoState {PRIZES, RULES, HIDDEN}

    private InfoState infoState = InfoState.PRIZES;
    private final Texture textureUi;
    private final Texture textureUi2;
    private final Texture textureButtons;
    private final Sprite background;
    private final Sprite backgroundTop;
    private static Sprite reelBack;
    private final Sprite reelFront;
    private final Sprite reelBottom;
    private final Sprite statsPanel;
    private final Sprite numberSprite;
    private final Sprite infoSprite;
    private final Sprite linesSprite;
    private final Sprite iconSprite;
    private final Sprite infoButton;
    private final Sprite menuButton;
    private final SpinButton spinButton;
    private final HoldButton holdButton0;
    private final HoldButton holdButton1;
    private final HoldButton holdButton2;
    private final Reel reel0;
    private final Reel reel1;
    private final Reel reel2;
    private final LightStrip lightStrip;
    private boolean spinning = false;
    private boolean stopping = false;
    private boolean playerWon = false;
    private long spinStartTime = 0;
    private long spinEndTime = 0;
    private int score = 200;
    private int holds = 0;

    public SlotsGame(GameScreen screen) {
        this.gameScreen = screen;

        // Textures
        textureUi = new Texture(Gdx.files.internal("art/ui.png"));
        textureUi2 = new Texture(Gdx.files.internal("art/ui2.png"));
        textureButtons = new Texture(Gdx.files.internal("art/slotButtons.png"));

        // UI - background and non-interactive
        background = new Sprite(textureUi);
        backgroundTop = new Sprite(textureUi);
        reelBack = new Sprite(textureUi);
        reelFront = new Sprite(textureUi);
        reelBottom = new Sprite(textureUi);
        statsPanel = new Sprite(textureUi);

        // TODO: A lot of these values are hardcoded for speed, but they should be in standardised variables and calculated using other variables etc
        background.setRegion(0, 0, 1920, 1080);
        background.setSize(1920, 1080);
        backgroundTop.setRegion(0, 0, 1920, 267);
        backgroundTop.setSize(1920, 267);
        reelBack.setRegion(0, 1080, 822, 547);
        reelBack.setSize(822, 547);
        reelFront.setRegion(822, 1080, 822, 547);
        reelFront.setSize(822, 547);
        reelBottom.setRegion(850, 1803, 827, 201);
        reelBottom.setSize(827, 201);
        statsPanel.setRegion(1644, 1080, 404, 547);
        statsPanel.setSize(404, 547);

        backgroundTop.setPosition(0, 1080 - 267);
        reelBack.setPosition((gameScreen.screenSize.x / 2) - (reelBack.getWidth() / 2), (gameScreen.screenSize.y / 2) - (reelBack.getHeight() / 2));
        reelFront.setPosition((gameScreen.screenSize.x / 2) - (reelFront.getWidth() / 2), (gameScreen.screenSize.y / 2) - (reelFront.getHeight() / 2));
        reelBottom.setPosition((gameScreen.screenSize.x / 2) - (reelFront.getWidth() / 2) - 3, (gameScreen.screenSize.y / 2) - (reelFront.getHeight() / 2) - 201);
        statsPanel.setPosition((gameScreen.screenSize.x / 2) + (reelFront.getWidth() / 2) + 10, (gameScreen.screenSize.y / 2) - (reelFront.getHeight() / 2));

        // UI - interactive or dynamic
        numberSprite = new Sprite(new Texture(Gdx.files.internal("art/numbers.png")));
        infoSprite = new Sprite(new Texture(Gdx.files.internal("art/info.png")));
        linesSprite = new Sprite(textureUi2);
        iconSprite = new Sprite(new Texture(Gdx.files.internal("art/icons.png")));
        infoButton = new Sprite(textureUi);
        menuButton = new Sprite(textureUi);
        spinButton = new SpinButton(textureButtons, getReelUiPosition(false, true));
        holdButton0 = new HoldButton(textureButtons, getReelPosition(0));
        holdButton1 = new HoldButton(textureButtons, getReelPosition(1));
        holdButton2 = new HoldButton(textureButtons, getReelPosition(2));

        numberSprite.setSize(95, 105); // Region set in loop
        infoSprite.setRegion(0, 0, 512, 732);
        infoSprite.setSize(512, 732);
        linesSprite.setRegion(0, 0, 822, 547);
        linesSprite.setSize(822, 547);
        iconSprite.setRegion(0, 0, 150, 150);
        iconSprite.setSize(150, 150);
        infoButton.setRegion(1830, 1627, 218, 206);
        infoButton.setSize(218, 206);
        menuButton.setRegion(1830, 1833, 218, 206);
        menuButton.setSize(218, 206);

        infoSprite.setPosition(15, 100);
        linesSprite.setPosition((gameScreen.screenSize.x / 2) - (linesSprite.getWidth() / 2), (gameScreen.screenSize.y / 2) - (linesSprite.getHeight() / 2));
        Vector2 pos = getReelUiPosition(true, false);
        infoButton.setPosition(32 + 218, 1080 - 206 - 32);
        menuButton.setPosition(1670, pos.y + 48);

        reel0 = new Reel(gameScreen, 0);
        reel1 = new Reel(gameScreen, 1);
        reel2 = new Reel(gameScreen, 2);

        lightStrip = new LightStrip(textureUi2, pos);
    }

    public void update(float delta, Vector2 mousePos) {
        // Input - Mouse or Touch
        if (mousePos != null) {
            // Buttons that can only be pressed when not spinning
            if (!spinning) {
                if (spinButton.contains(mousePos)) {
                    gameScreen.playSound(gameScreen.confirmSound, false);
                    spin();
                }
                if (!playerWon && checkHoldsAvailable()) {
                    if (holdButton0.contains(mousePos) && holdButton0.isPressable()) {
                        holdButton0.setState(Button.State.PRESSED);
                        holdReel(0);
                    }
                    if (holdButton1.contains(mousePos) && holdButton1.isPressable()) {
                        holdButton1.setState(Button.State.PRESSED);
                        holdReel(1);
                    }
                    if (holdButton2.contains(mousePos) && holdButton2.isPressable()) {
                        holdButton2.setState(Button.State.PRESSED);
                        holdReel(2);
                    }
                }
            }


            // Buttons that can be pressed whenever (menu button? audio button if I get that far?)
            if (infoButton.getBoundingRectangle().contains(mousePos)) {
                gameScreen.playSound(gameScreen.confirmSound, false);
                changeInfo();
            }
            if (menuButton.getBoundingRectangle().contains(mousePos)) {
                gameScreen.playSound(gameScreen.confirmSound, false);
                gameScreen.backToMenu();
            }
        }

        reel0.update(delta);
        reel1.update(delta);
        reel2.update(delta);
        lightStrip.update();

        if (spinning && !stopping) {
            if (TimeUtils.nanoTime() - spinStartTime > spinEndTime) stopSpin();
        }

        if (spinning && stopping) {
            if (!reel0.isSpinning() && !reel1.isSpinning() && !reel2.isSpinning()) {
                stopping = false;
                spinning = false;
                gameScreen.spinSound.stop();
                lightStrip.changeState(LightStrip.State.STANDBY);
                spinButton.setState(Button.State.LIT);
                handleResults();
                resetReelHolds();
            }
        } else if (stopping) {
            System.out.println("Error: stopping but not spinning!");
            assert (false);
        }
    }

    private void spin() {
        score -= 20;
        spinning = true;
        spinButton.setState(Button.State.PRESSED);
        spinStartTime = TimeUtils.nanoTime();
        spinEndTime = (long) ((2 + Math.random()) * 1000000000); // Sets a random time between 2-3 seconds

        gameScreen.playSound(gameScreen.spinSound, true);
        lightStrip.changeState(LightStrip.State.SPINNING);

        reel0.start();
        reel1.start();
        reel2.start();
        updateHoldButtons();
    }

    private void stopSpin() {
        stopping = true;
        reel0.stop();
        reel1.stop();
        reel2.stop();
    }

    private void holdReel(int id) {
        switch (id) {
            case 0:
                reel0.hold(true);
                break;
            case 1:
                reel1.hold(true);
                break;
            case 2:
                reel2.hold(true);
                break;
            default:
                assert (false);
                break;
        }

        gameScreen.playSound(gameScreen.holdSound, false);
        holds -= 1;
        updateHoldButtons();
    }

    private void resetReelHolds() {
        reel0.hold(false);
        reel1.hold(false);
        reel2.hold(false);

        if (playerWon) {
            holdButton0.setState(Button.State.DIM);
            holdButton1.setState(Button.State.DIM);
            holdButton2.setState(Button.State.DIM);
        } else
            updateHoldButtons();
    }

    private boolean checkHoldsAvailable() {
        if (holds > 0) {
            int counter = 0;

            if (reel0.isHeld()) counter++;
            if (reel1.isHeld()) counter++;
            if (reel2.isHeld()) counter++;

            return counter < 2;
        } else
            return false;
    }

    private void updateHoldButtons() {
        if (spinning) {
            if (!reel0.isHeld())
                holdButton0.setState(Button.State.DIM);
            if (!reel1.isHeld())
                holdButton1.setState(Button.State.DIM);
            if (!reel2.isHeld())
                holdButton2.setState(Button.State.DIM);
        } else if (checkHoldsAvailable()) {
            if (!reel0.isHeld())
                holdButton0.setState(Button.State.LIT);
            if (!reel1.isHeld())
                holdButton1.setState(Button.State.LIT);
            if (!reel2.isHeld())
                holdButton2.setState(Button.State.LIT);
        } else {
            if (!reel0.isHeld())
                holdButton0.setState(Button.State.DIM);
            if (!reel1.isHeld())
                holdButton1.setState(Button.State.DIM);
            if (!reel2.isHeld())
                holdButton2.setState(Button.State.DIM);
        }
    }

    // Checks all possible combinations for wins
    private void handleResults() {
        // This is probably going to be a lot of ifs
        boolean won000 = false;
        boolean won111 = false;
        boolean won222 = false;
        boolean won012 = false;
        boolean won210 = false;
        Vector3 result0 = reel0.getWinningIcons();
        Vector3 result1 = reel1.getWinningIcons();
        Vector3 result2 = reel2.getWinningIcons();
        int[][] wIcons = {
            {(int) result0.x, (int) result0.y, (int) result0.z},
            {(int) result1.x, (int) result1.y, (int) result1.z},
            {(int) result2.x, (int) result2.y, (int) result2.z}
        };

        // Check various fruit only win conditions
        if (wIcons[0][0] == wIcons[1][0] && wIcons[1][0] == wIcons[2][0]) {
            won000 = true;
            win(wIcons[0][0]);
        }
        if (wIcons[0][1] == wIcons[1][1] && wIcons[1][1] == wIcons[2][1]) {
            won111 = true;
            win(wIcons[0][1]);
        }
        if (wIcons[0][2] == wIcons[1][2] && wIcons[1][2] == wIcons[2][2]) {
            won222 = true;
            win(wIcons[0][2]);
        }
        if (wIcons[0][0] == wIcons[1][1] && wIcons[1][1] == wIcons[2][2]) {
            won012 = true;
            win(wIcons[0][0]);
        }
        if (wIcons[0][2] == wIcons[1][1] && wIcons[1][1] == wIcons[2][0]) {
            won210 = true;
            win(wIcons[0][2]);
        }

        // Find wilds
        boolean[][] wilds = {
            {false, false, false},
            {false, false, false},
            {false, false, false}
        };
        boolean wildsOnScreen = false;

        if (wIcons[0][0] == 0) {
            wilds[0][0] = true;
            wildsOnScreen = true;
        }
        if (wIcons[0][1] == 0) {
            wilds[0][1] = true;
            wildsOnScreen = true;
        }
        if (wIcons[0][2] == 0) {
            wilds[0][2] = true;
            wildsOnScreen = true;
        }
        if (wIcons[1][0] == 0) {
            wilds[1][0] = true;
            wildsOnScreen = true;
        }
        if (wIcons[1][1] == 0) {
            wilds[1][1] = true;
            wildsOnScreen = true;
        }
        if (wIcons[1][2] == 0) {
            wilds[1][2] = true;
            wildsOnScreen = true;
        }
        if (wIcons[2][0] == 0) {
            wilds[2][0] = true;
            wildsOnScreen = true;
        }
        if (wIcons[2][1] == 0) {
            wilds[2][1] = true;
            wildsOnScreen = true;
        }
        if (wIcons[2][2] == 0) {
            wilds[2][2] = true;
            wildsOnScreen = true;
        }

        // If there's a wild we need to check all conditions for each wild
        if (wildsOnScreen) {
            // Now any win bool could be true, so we need to make further checks
            if (wilds[0][0]) {
                if (!won000) {
                    if (wIcons[1][0] == wIcons[2][0]) {
                        won000 = true;
                        win(wIcons[1][0]);
                    } else if (wilds[1][0]) {
                        won000 = true;
                        win(wIcons[2][0]);
                    } else if (wilds[2][0]) {
                        won000 = true;
                        win(wIcons[1][0]);
                    }
                }

                if (!won012) {
                    if (wIcons[1][1] == wIcons[2][2]) {
                        won012 = true;
                        win(wIcons[1][1]);
                    } else if (wilds[1][1]) {
                        won012 = true;
                        win(wIcons[2][2]);
                    } else if (wilds[2][2]) {
                        won012 = true;
                        win(wIcons[1][1]);
                    }
                }
            }
            if (wilds[0][1]) {
                if (!won111) {
                    if (wIcons[1][1] == wIcons[2][1]) {
                        won111 = true;
                        win(wIcons[1][1]);
                    } else if (wilds[1][1]) {
                        won111 = true;
                        win(wIcons[2][1]);
                    } else if (wilds[2][1]) {
                        won111 = true;
                        win(wIcons[1][1]);
                    }
                }
            }
            if (wilds[0][2]) {
                if (!won222) {
                    if (wIcons[1][2] == wIcons[2][2]) {
                        won222 = true;
                        win(wIcons[1][2]);
                    } else if (wilds[1][2]) {
                        won222 = true;
                        win(wIcons[2][2]);
                    } else if (wilds[2][2]) {
                        won222 = true;
                        win(wIcons[1][2]);
                    }
                }

                if (!won210) {
                    if (wIcons[1][1] == wIcons[2][0]) {
                        won210 = true;
                        win(wIcons[1][1]);
                    } else if (wilds[1][1]) {
                        won210 = true;
                        win(wIcons[2][0]);
                    } else if (wilds[2][0]) {
                        won210 = true;
                        win(wIcons[1][1]);
                    }
                }
            }
            // We've already checked the first row for wilds, so now we don't need to
            if (wilds[1][0]) {
                if (!won000) {
                    if (wIcons[0][0] == wIcons[2][0] || wilds[2][0]) {
                        won000 = true;
                        win(wIcons[0][0]);
                    }
                }
            }
            if (wilds[1][1]) {
                if (!won111) {
                    if (wIcons[0][1] == wIcons[2][1] || wilds[2][1]) {
                        won111 = true;
                        win(wIcons[0][1]);
                    }
                }
                if (!won012) {
                    if (wIcons[0][0] == wIcons[2][2] || wilds[2][2]) {
                        won012 = true;
                        win(wIcons[0][0]);
                    }
                }
                if (!won210) {
                    if (wIcons[0][2] == wIcons[2][0] || wilds[2][0]) {
                        won210 = true;
                        win(wIcons[0][2]);
                    }
                }
            }
            if (wilds[1][2]) {
                if (!won222) {
                    if (wIcons[0][2] == wIcons[2][2] || wilds[2][2]) {
                        won222 = true;
                        win(wIcons[0][2]);
                    }
                }
            }
            // Now we're onto the last row we don't need to check for any wilds, just fruits
            if (wilds[2][0]) {
                if (!won000) {
                    if (wIcons[0][0] == wIcons[1][0]) {
                        won000 = true;
                        win(wIcons[0][0]);
                    }
                }
                if (!won210) {
                    if (wIcons[0][2] == wIcons[1][1]) {
                        won210 = true;
                        win(wIcons[0][2]);
                    }
                }
            }
            if (wilds[2][1]) {
                if (!won111) {
                    if (wIcons[0][1] == wIcons[1][1]) {
                        won111 = true;
                        win(wIcons[0][1]);
                    }
                }
            }
            if (wilds[2][2]) {
                if (!won222) {
                    if (wIcons[0][2] == wIcons[1][2]) {
                        won222 = true;
                        win(wIcons[0][2]);
                    }
                }
                if (!won012) {
                    if (wIcons[0][0] == wIcons[1][1]) {
                        won012 = true;
                        win(wIcons[0][0]);
                    }
                }
            }
        }
        // There has to be a better way to do this, but for now it'll do

        if (won000 || won111 || won222 || won012 || won210) {
            gameScreen.playSound(gameScreen.winSound, false);
            lightStrip.changeState(LightStrip.State.WIN);
            playerWon = true;
        } else {
            gameScreen.playSound(gameScreen.loseSound, false);
            lightStrip.changeState(LightStrip.State.LOSE);
            playerWon = false;
        }

        if (wildsOnScreen && !playerWon)
            addHolds(wilds);
    }

    private void win(int id) {
        switch (id) {
            case 0:
                score += 200;
                break;
            case 1:
                score += 150;
                break;
            case 2:
                score += 120;
                break;
            case 3:
                score += 100;
                break;
            case 4:
                score += 80;
                break;
            case 5:
                score += 60;
                break;
            case 6:
                score += 50;
                break;
            case 7:
                score += 30;
                break;
            case 8:
                score += 20;
                break;
            default:
                assert (false);
                break;
        }
    }

    private void addHolds(boolean[][] wilds) {
        if (!reel0.isHeld()) {
            if (wilds[0][0]) holds++;
            if (wilds[0][1]) holds++;
            if (wilds[0][2]) holds++;
        }
        if (!reel1.isHeld()) {
            if (wilds[1][0]) holds++;
            if (wilds[1][1]) holds++;
            if (wilds[1][2]) holds++;
        }
        if (!reel2.isHeld()) {
            if (wilds[2][0]) holds++;
            if (wilds[2][1]) holds++;
            if (wilds[2][2]) holds++;
        }

        updateHoldButtons();
    }

    // Called by gameScreen mid-batch
    public void render(SpriteBatch batch) {
        batch.disableBlending();
        background.draw(batch);
        batch.enableBlending();

        reelBack.draw(batch);
        reel0.render(batch, iconSprite);
        reel1.render(batch, iconSprite);
        reel2.render(batch, iconSprite);

        // To cover up reel icons
        batch.disableBlending();
        backgroundTop.draw(batch);
        batch.enableBlending();

        reelFront.draw(batch);
        reelBottom.draw(batch);

        if (infoState != InfoState.HIDDEN)
            linesSprite.draw(batch);

        statsPanel.draw(batch);
        drawNumber(batch, score, false);
        drawNumber(batch, holds, true);

        infoSprite.draw(batch);
        infoButton.draw(batch);
        menuButton.draw(batch);

        spinButton.sprite.draw(batch);
        holdButton0.sprite.draw(batch);
        holdButton1.sprite.draw(batch);
        holdButton2.sprite.draw(batch);

        lightStrip.render(batch);
    }

    // Rotates the information shown between 3 states
    private void changeInfo() {
        if (infoState == InfoState.PRIZES) {
            infoState = InfoState.RULES;
            infoSprite.setRegion(0, 716, 512, 732);
        } else if (infoState == InfoState.RULES) {
            infoState = InfoState.HIDDEN;
            infoSprite.setRegion(0, 732 * 2, 512, 1);
        } else {
            infoState = InfoState.PRIZES;
            infoSprite.setRegion(0, 0, 512, 732);
        }
    }

    private void drawNumber(SpriteBatch batch, int value, boolean drawingHolds) {
        Vector2 startPos = new Vector2(statsPanel.getX() + 12, statsPanel.getY() + statsPanel.getHeight() - 105 - 130);
        if (drawingHolds)
            startPos.y -= 240;

        // Adding this so it doesn't break if someone gets over 9999 or less than 0
        int temp = Math.min(value, 9999);
        if (temp < 0)
            temp = 0;

        int modulus = 10000;
        boolean firstNumber = true;
        for (int i = 3; i != -1; --i) {
            int divider = (int) Math.pow(10, i);
            int digit = (temp % modulus) / divider;
            modulus = divider;

            if (digit != 0 || i == 0 || !firstNumber) {
                numberSprite.setRegion(95 * digit, 0, 95, 105);
                numberSprite.setPosition(startPos.x + (95 * (3 - i)), startPos.y);
                numberSprite.draw(batch);
                firstNumber = false;
            }
        }
    }

    // Returns an individual reel position, relative to the reel UI
    public static Vector2 getReelPosition(int id) {
        Vector2 pos;

        switch (id) {
            case 0:
                pos = new Vector2(reelBack.getX() + 95.f, reelBack.getY() - 100.f);
                break;
            case 1:
                pos = new Vector2(reelBack.getX() + 335.f, reelBack.getY() - 100.f);
                break;
            case 2:
                pos = new Vector2(reelBack.getX() + 575.f, reelBack.getY() - 100.f);
                break;
            default:
                pos = null;
                assert (false);
                break;
        }

        return pos;
    }

    // Returns a reel UI position, for relatively positioning UI
    private Vector2 getReelUiPosition(boolean top, boolean right) {
        Vector2 pos = new Vector2(reelBack.getX(), reelBack.getY());
        if (top) {
            pos.y += reelBack.getHeight();
        }
        if (right) {
            pos.x += reelBack.getWidth();
        }
        return pos;
    }

    public void dispose() {
        reel0.dispose();
        reel1.dispose();
        reel2.dispose();
        textureUi.dispose();
        textureUi2.dispose();
        textureButtons.dispose();
    }
}
