package tomchapdev.slots.gameplay;

import java.util.*;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import tomchapdev.slots.states.GameScreen;

public class Reel {
    private final GameScreen gameScreen;
    private final int numIcons = 9;
    private final int numRepeats = 3;
    private final int iconsInReel = numIcons * numRepeats;
    private final int numberOfDrawnIcons = 4;

    private final int[] icons;
    private float spinPosition = 0.f;
    private int stopPosition = -1;
    private final int reelId;
    private boolean held = false;
    private boolean spinning = false;
    private boolean stopping = false;

    public Reel(GameScreen screen, int pId) {
        this.gameScreen = screen;
        icons = new int[iconsInReel];
        generateRandomReel();

        reelId = pId;
    }

    public void update(float delta) {
        if (spinning) {
            // 8 icons per second
            final float spinSpeed = 16.f;
            spinPosition += spinSpeed * delta;

            // At stop position so stop update
            if (stopping && (Math.floor(spinPosition) == stopPosition)) {
                //System.out.println("STOPPED! Reel Id: " + reelId + ", spinPosition: " + spinPosition + ", stopPosition: " + stopPosition);
                stopping = false;
                spinning = false;
                spinPosition = (float) stopPosition;
                gameScreen.playSound(gameScreen.stopSound, false);
                return;
            }

            // Clamp spin position to icons max index
            if (spinPosition > iconsInReel - 1) {
                spinPosition -= iconsInReel - 1;
            }
        }
    }

    public void hold(boolean bool) {
        held = bool;
    }

    // Based on spinPosition, draw icons to the reelBuffer
    public void render(SpriteBatch batch, Sprite sprite) {
        final int iconSpriteSize = 150;

        for (int i = 0; i < numberOfDrawnIcons; ++i) {
            // Find icon ID
            int temp = (int) Math.floor(spinPosition) - i;
            if (temp < 0) {
                temp += iconsInReel - 1;
            }
            int iconId = icons[temp];
            sprite.setRegion(iconSpriteSize * (iconId % 3),
                iconSpriteSize * (iconId / 3), iconSpriteSize, iconSpriteSize);

            // Set position and draw
            Vector2 pos = SlotsGame.getReelPosition(reelId);
            pos.y += (iconSpriteSize * i) + (iconSpriteSize * (spinPosition % 1));
            sprite.setPosition(pos.x, pos.y);
            sprite.draw(batch);
        }
    }

    public void start() {
        if (!held) {
            spinning = true;
            stopping = false;
        }
    }

    public void stop() {
        if (!held) {
            stopping = true;
            stopPosition = (int) Math.ceil(spinPosition) + (reelId * 8);

            // Clamp stop position to icons max index
            if (stopPosition > (iconsInReel - 1)) {
                stopPosition -= iconsInReel - 1;
            }
            //System.out.println("STOPPING! Reel ID: " + reelId + ", stop Position: " + stopPosition);
        }
    }

    public boolean isSpinning() {
        return spinning;
    }

    public boolean isHeld() { return held; }

    public Vector3 getWinningIcons() {
        Vector3 results = new Vector3();
        for (int i = 1; i < numberOfDrawnIcons; ++i) {
            // Find icon ID
            int temp = (int) spinPosition - i;
            if (temp < 0) {
                temp += iconsInReel - 1;
            }

            switch (i) {
                case 1:
                    results.x = icons[temp];
                    break;
                case 2:
                    results.y = icons[temp];
                    break;
                case 3:
                    results.z = icons[temp];
                    break;
                default:
                    assert (false);
                    break;
            }
        }

        return results;
    }

    // Generate a unique evenly distributed array of icons
    private void generateRandomReel() {
        int counter = 0;
        while (counter < numRepeats) {
            // Create an array of icon IDs
            int[] intArray = new int[numIcons];
            for (int i = 0; i < intArray.length; ++i) {
                intArray[i] = i;
            }

            // Shuffle array to ensure even distribution of icons
            shuffleArray(intArray);

            // Check if the previous array's last value and the current array's first value are the same
            if (counter != 0) {
                if (icons[(counter * numIcons) - 1] == intArray[0]) {
                    // Swap the first and second elements of the new array section
                    int temp = icons[counter * numIcons];
                    icons[counter * numIcons] = icons[(counter * numIcons) + 1];
                    icons[(counter * numIcons) + 1] = temp;
                }
            }

            // Set the new values of the icons array
            System.arraycopy(intArray, 0, icons, counter * numIcons, intArray.length);
            ++counter;
        }
    }

    // Implementing Fisher–Yates shuffle, copied this from online after trying the collections shuffle
    public static void shuffleArray(int[] array) {
        Random random = new Random();

        // Start from the last element and swap one by one
        for (int i = array.length - 1; i > 0; i--) {
            // Generate a random index between 0 and i (inclusive)
            int randomIndex = random.nextInt(i + 1);

            // Swap array[i] with the element at randomIndex
            int temp = array[i];
            array[i] = array[randomIndex];
            array[randomIndex] = temp;
        }
    }

    public void dispose() {
    }
}
