package tomchapdev.slots.states;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.ScreenUtils;

import tomchapdev.slots.DefaultScreen;

public class MenuScreen extends DefaultScreen {
    private final SpriteBatch batch;
    private final Texture textureUi;
    private final Sprite background;
    private final Sprite title;
    private final Sprite playButton;
    private final Sprite quitButton;
    private final Sprite credits;
    boolean switchToGame;
    private float lastMouseInput = 0.f;

    public MenuScreen(Game game, AudioState currentAudioState) {
        super(game, true, currentAudioState);
        audioButton.setPosition(32, 32);
        batch = new SpriteBatch();

        textureUi = new Texture(Gdx.files.internal("art/ui.png"));
        background = new Sprite(textureUi);
        title = new Sprite(textureUi);
        playButton = new Sprite(textureUi);
        quitButton = new Sprite(textureUi);
        credits = new Sprite(new Texture(Gdx.files.internal("art/info.png")));
        // Sprite texture regions
        background.setRegion(0, 0, 1920, 1080);
        background.setSize(1920, 1080);
        title.setRegion(0, 1627, 1803, 175);
        title.setSize(1803, 175);
        playButton.setRegion(0, 1803, 425, 175);
        playButton.setSize(425, 175);
        quitButton.setRegion(425, 1803, 425, 175);
        quitButton.setSize(425, 175);
        credits.setRegion(0, 1500, 512, 306);
        credits.setSize(512, 306);
        // FIXME positions need to be clamped to the screen but still positioned nicely, depending on actual screen size
        title.setPosition((screenSize.x / 2) - (title.getWidth() / 2), (screenSize.y / 2) + playButton.getHeight());
        playButton.setPosition((screenSize.x / 2) - (playButton.getWidth() / 2), (screenSize.y / 2) - playButton.getHeight());
        quitButton.setPosition((screenSize.x / 2) - (quitButton.getWidth() / 2), (screenSize.y / 2) - (quitButton.getHeight() * 2.5f));
        credits.setPosition(screenSize.x - 528, 16);

        switchToGame = false;
    }

    // Stores all game logic to separate it from render
    private void update(float delta) {
        lastMouseInput += delta;

        // Input - Mouse or Touch
        if (Gdx.input.isTouched()) {
            // Get position and converts it to world space
            Vector3 inputPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
            viewport.getCamera().unproject(inputPos, viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());
            Vector2 mousePos = new Vector2(inputPos.x, inputPos.y);

            // Check if click/tap is on sprites
            if (playButton.getBoundingRectangle().contains(mousePos)) {
                playSound(confirmSound, false);
                game.setScreen(new GameScreen(game, audioState));
                dispose();
                switchToGame = true;
            }
            if (quitButton.getBoundingRectangle().contains(mousePos))
                Gdx.app.exit();

            // To reduce spam inputs on toggle buttons
            if (lastMouseInput > 0.33f) {
                lastMouseInput = 0.f;

                if (audioButton.getBoundingRectangle().contains(mousePos)) {
                    toggleAudio();
                    playSound(confirmSound, false);
                }
            }
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK) || Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE))
            Gdx.app.exit();
    }

    @Override
    public void render(float delta) {
        update(delta);

        if (!switchToGame) {
            ScreenUtils.clear(0, 0, 0, 1);
            viewport.getCamera().update();
            batch.setProjectionMatrix(viewport.getCamera().combined);

            batch.begin();
            background.draw(batch);
            title.draw(batch);
            playButton.draw(batch);

            if (Gdx.app.getType() != Application.ApplicationType.WebGL)
                quitButton.draw(batch);

            audioButton.draw(batch);
            credits.draw(batch);
            batch.end();
        }
    }

    @Override
    public void dispose() {
        textureUi.dispose();
        batch.dispose();
        confirmSound.dispose();
        stopSound.dispose();
        winSound.dispose();
        loseSound.dispose();
        spinSound.dispose();
        holdSound.dispose();
        bgMusic.dispose();
        game.dispose();
    }
}
