package tomchapdev.slots.states;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.ScreenUtils;

import tomchapdev.slots.DefaultScreen;
import tomchapdev.slots.gameplay.SlotsGame;

public class GameScreen extends DefaultScreen {
    private final SpriteBatch batch;
    private final SlotsGame slots;
    private static boolean switchToMenu;
    private float lastMouseInput = 0.f;

    public GameScreen(Game game, AudioState currentAudioState) {
        super(game, false, currentAudioState);
        audioButton.setPosition(32, 1080 - 206 - 32);

        batch = new SpriteBatch();
        slots = new SlotsGame(this);

        switchToMenu = false;
    }

    private void update(float delta) {
        Vector2 mousePos = null;
        lastMouseInput += delta;

        // Input - Mouse or Touch
        if (Gdx.input.isTouched()) {
            // To reduce spam inputs on buttons
            if (lastMouseInput > 0.33f) {
                lastMouseInput = 0.f;

                // Get position and converts it to world space
                Vector3 inputPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
                viewport.getCamera().unproject(inputPos, viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());
                mousePos = new Vector2(inputPos.x, inputPos.y);

                if (audioButton.getBoundingRectangle().contains(mousePos)) {
                    toggleAudio();
                    playSound(confirmSound, false);
                }
            }
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK) || Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE))
            switchToMenu = true;

        slots.update(delta, mousePos);

        if (switchToMenu) {
            spinSound.stop();
            playSound(confirmSound, false);
            game.setScreen(new MenuScreen(game, audioState));
            dispose();
        }
    }

    public void backToMenu() {
        switchToMenu = true;
    }

    @Override
    public void render(float delta) {
        // Clamp delta to 1/60 (60fps) for debugging
        delta = Math.min(delta, 1.f / 60.f);

        update(delta);

        if (!switchToMenu) {
            ScreenUtils.clear(0, 0, 0, 1);
            viewport.getCamera().update();
            batch.setProjectionMatrix(viewport.getCamera().combined);

            batch.begin();
            slots.render(batch);
            audioButton.draw(batch);
            batch.end();
        }
    }

    @Override
    public void dispose() {
        batch.dispose();
        slots.dispose();
        confirmSound.dispose();
        stopSound.dispose();
        winSound.dispose();
        loseSound.dispose();
        spinSound.dispose();
        holdSound.dispose();
        bgMusic.dispose();
        game.dispose();
    }
}
