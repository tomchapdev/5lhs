package tomchapdev.slots.lwjgl3;

import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.math.Vector2;

import tomchapdev.slots.SlotMachine;

/** Launches the desktop (LWJGL3) application. */
public class Lwjgl3Launcher {
    public static void main(String[] args) {
        if (StartupHelper.startNewJvmIfRequired()) return; // This handles macOS support and helps on Windows.
        createApplication();
    }

    private static Lwjgl3Application createApplication() {
        return new Lwjgl3Application(new SlotMachine(), getDefaultConfiguration());
    }

    private static Lwjgl3ApplicationConfiguration getDefaultConfiguration() {
        Lwjgl3ApplicationConfiguration configuration = new Lwjgl3ApplicationConfiguration();
        configuration.setTitle("5LHS");
        configuration.useVsync(true);
        //// Limits FPS to the refresh rate of the currently active monitor.
        configuration.setForegroundFPS(Lwjgl3ApplicationConfiguration.getDisplayMode().refreshRate);
        //// If you remove the above line and set Vsync to false, you can get unlimited FPS, which can be
        //// useful for testing performance, but can also be very stressful to some hardware.
        //// You may also need to configure GPU drivers to fully disable Vsync; this can cause screen tearing.

        Graphics.DisplayMode primaryMode = Lwjgl3ApplicationConfiguration.getDisplayMode();
        Vector2 size = new Vector2();
        Vector2 pos = new Vector2();
        Vector2 offset = new Vector2(160, 90);

        // Ensures window is always 1080p or slightly smaller than the full screen, depending on current display size
        if (primaryMode.width > 1920 && primaryMode.height > 1080) {
            size.x = 1920;
            size.y = 1080;
            pos.x = (primaryMode.width - 1920) / 2.f;
            pos.y = (primaryMode.height - 1080) / 2.f;
        } else if (primaryMode.width == 1920 && primaryMode.height == 1080) {
            size.x = 1920 - offset.x;
            size.y = 1080 - offset.y;
            pos.x = offset.x / 2;
            pos.y = offset.y / 2;
        } else { // Smaller than 1080p so we want to have a smaller offset
            offset.x /= 2;
            offset.y /= 2;
            size.x = primaryMode.width - offset.x;
            size.y = primaryMode.height - offset.y;
            pos.x = offset.x / 2;
            pos.y = offset.y / 2;
        }

        configuration.setWindowedMode((int)size.x, (int)size.y);
        configuration.setWindowPosition((int)pos.x, (int)pos.y);
        configuration.setWindowIcon("libgdx128.png", "libgdx64.png", "libgdx32.png", "libgdx16.png");

        return configuration;
    }
}
